package basepack;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ProjectSpec {
	
	public static WebDriver driver;
	public static Properties pro;

	@BeforeMethod
	public void launchApplication() throws IOException {
		
		FileInputStream fis = new FileInputStream("./src/main/resources/LocatorsInAllPages.Properties");
		pro = new Properties();
		pro.load(fis);
		WebDriverManager.chromedriver().setup();
		ChromeOptions opt = new ChromeOptions();
		opt.addArguments("--disable-notifications");
		driver = new ChromeDriver(opt);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://login.salesforce.com/");

	}
	
	@AfterMethod
	public void closeBrowser() {
		driver.close();
	}
	
	public void locElement() {
		

	}
	

}
