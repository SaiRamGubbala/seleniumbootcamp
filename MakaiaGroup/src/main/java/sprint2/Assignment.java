package sprint2;

import java.time.Duration;

import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Assignment {

	public static void main(String[] args) throws InterruptedException {

		String createDashboardName = "Sai Ram_Workout";
		WebDriverManager.chromedriver().setup();
		ChromeOptions opt = new ChromeOptions();
		opt.addArguments("--disable-notifications");
		WebDriver driver = new ChromeDriver(opt);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

		// Login
		driver.get("https://login.salesforce.com/");
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");
		driver.findElement(By.id("Login")).click();
		Thread.sleep(4000);

		// Click App launcher
		driver.findElement(By.className("slds-icon-waffle")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[@class='slds-button']")).click();
		Thread.sleep(3000);

		// Searching Service Console and select
		driver.findElement(By.xpath("//input[@type='search' and @class='slds-input']")).sendKeys("Service Console");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//p[@class='slds-truncate']")).click();
		Thread.sleep(8000);

		// selecting home from dropdown
		driver.findElement(By.xpath("//div[@class='oneConsoleNav navexConsoleNav']/div[3]/div/button")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@data-itemid='Dashboard']")).click();
		Thread.sleep(4000);

		// New Dashboard click
		driver.findElement(By.xpath("//div[@title='New Dashboard']")).click();
		// Thread.sleep(4000);

		// Switch to frame
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		// Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@class='slds-input' and @id='dashboardNameInput']"))
				.sendKeys(createDashboardName);
		driver.findElement(By.xpath("//input[@class='slds-input' and @id='dashboardDescriptionInput']"))
				.sendKeys("Testing");
		driver.findElement(By.id("submitBtn")).click();
		Thread.sleep(2000);
		driver.switchTo().defaultContent();

		// Clicking the Done
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		driver.findElement(By.xpath("//button[@type='button' and text()='Done']")).click();
		Thread.sleep(3000);
		driver.switchTo().defaultContent();

		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
		String dashBoardName = driver.findElement(By.xpath("//span[@class='slds-page-header__title slds-truncate']"))
				.getText();
		// System.out.println(dashBoardName);
		if (dashBoardName.equalsIgnoreCase(createDashboardName))
			System.out.println(dashBoardName + " created Sucessfully");
		else
			System.out.println(createDashboardName + " not created Successfully");
		driver.findElement(By.xpath("//button[text()='Subscribe']")).click();
		Thread.sleep(3000);
		driver.switchTo().defaultContent();

		driver.findElement(By.xpath("//span[text()='Daily']")).click();
		Thread.sleep(2000);
		WebElement timeDropDown = driver.findElement(By.id("time"));
		Select timeSelection = new Select(timeDropDown);
		timeSelection.selectByVisibleText("10:00 AM");

		Thread.sleep(3000);
		driver.findElement(By.xpath("//span[text()='Save']")).click();
		Thread.sleep(3000);

		String subscriptionMessage = driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']"))
				.getText();

		if (subscriptionMessage.equalsIgnoreCase("You started a dashboard subscription."))
			System.out.println(subscriptionMessage + " message displayed");
		else
			System.out.println(subscriptionMessage + " message not displayed");

		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@title='" + createDashboardName + "']/following-sibling::div[2]")).click();
		// label[@for='time']/following-sibling::select

		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@class='slds-nav-vertical__action' and @title='Private Dashboards']")).click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']"))
				.sendKeys(createDashboardName);
		Thread.sleep(3000);

		String searchCreatedDashboardName = driver.findElement(By.xpath("//a[@title='" + createDashboardName + "']"))
				.getText();
		if (searchCreatedDashboardName.equalsIgnoreCase(createDashboardName)) {
			System.out.println(createDashboardName + " is created and available in Private Dashboards");
			driver.findElement(By.xpath("//table[@role='grid']//tr[1]/td[6]/lightning-primitive-cell-factory/span/div"))
					.click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//span[text()='Delete']")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("//div[@class='modal-footer slds-modal__footer']//button[2]/span")).click();

			Thread.sleep(5000);
			driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']")).clear();
			driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']"))
					.sendKeys(createDashboardName);

			String noResultsMessage = driver.findElement(By.xpath("//span[text()='No results found']")).getText();
			System.out.println(noResultsMessage);
			Thread.sleep(3000);
			if (searchCreatedDashboardName.equalsIgnoreCase(noResultsMessage))
				System.out.println("Dashboard Not Deleted Successfully");
			else
				System.out.println("Dashboard Deleted successfully");

		} else {
			System.out.println(createDashboardName + " is not created in Private Dashboards");
		}

		Thread.sleep(5000);
		driver.quit();

	}

}
