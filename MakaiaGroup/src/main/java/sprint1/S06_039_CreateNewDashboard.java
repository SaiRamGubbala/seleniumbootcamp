package sprint1;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class S06_039_CreateNewDashboard {

	public static void main(String[] args) throws InterruptedException {
				
				// Chrome Browser setup using WebDriver Manager
				WebDriverManager.chromedriver().setup();

				// disable the chrome notifications
				ChromeOptions opt = new ChromeOptions();
				opt.addArguments("--disable-notifications");

				// Webdriver Setup by passing chromeoptions
				WebDriver driver = new ChromeDriver(opt);

				// Maximise the Browser Window
				driver.manage().window().maximize();

				// WebDriver wait initialization before the exception
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

				// Get the URL
				driver.get("https://login.salesforce.com/");

				// Enter the Username in the login page
				driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");

				// Enter the Password in the login page
				driver.findElement(By.id("password")).sendKeys("BootcampSel$123");

				// Click the LogIn button from login page
				driver.findElement(By.id("Login")).click();

				// click the App launcher
				driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();

				WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));

				// Click View All option from the App Launcher expansion
				driver.findElement(By.xpath("//button[@class='slds-button']")).click();
				
				//Search Dashboards and Click the Dashboards
				driver.findElement(By.xpath("//input[@type='search' and @class='slds-input']")).sendKeys("Dashboards");
				driver.findElement(By.xpath("//p[@class='slds-truncate']")).click();
				
				//Click on the New Dashboard option
				driver.findElement(By.xpath("//a[@title='New Dashboard']")).click();
				
				//Switching to Frame where New Dashboard pop up details available
				driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
				
				//Entering Dashboard Name and Clicking Create button
				driver.findElement(By.id("dashboardNameInput")).sendKeys("Salesforce Automation by Sai Ram");
				driver.findElement(By.id("submitBtn")).click();
				
				//Switching from frame to default content
				driver.switchTo().defaultContent();
				
				Thread.sleep(3000); 
				
				//Switching to Frame to click the Save button
				driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
				
				Thread.sleep(4000); 
				
				//Clicking the Save button
				driver.findElement(By.xpath("//button[@type='button' and text()='Save']")).click();
				 
			    //Switching to default content 
				driver.switchTo().defaultContent();
				
				//Getting the toast message of whether Dashboard created or not
				Thread.sleep(2000);
				 
				//Getting the Dashboard created text and displaying
				System.out.println(driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText());
				
				Thread.sleep(8000); 
				//Closing Browser
				driver.quit();
				 


	}

}
