package sprint1;

import java.time.Duration;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class S06_055_EditCase {
	
	public static void main(String[] args) throws InterruptedException {
		// Chrome Browser setup using WebDriver Manager
				WebDriverManager.chromedriver().setup();

				// disable the chrome notifications
				ChromeOptions opt = new ChromeOptions();
				opt.addArguments("--disable-notifications");

				// Webdriver Setup by passing chromeoptions
				WebDriver driver = new ChromeDriver(opt);

				// Maximise the Browser Window
				driver.manage().window().maximize();

				// WebDriver wait initialization before the exception
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

				// Get the URL
				driver.get("https://login.salesforce.com/");

				// Enter the Username in the login page
				driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");

				// Enter the Password in the login page
				driver.findElement(By.id("password")).sendKeys("BootcampSel$123");

				// Click the LogIn button from login page
				driver.findElement(By.id("Login")).click();
				
				WebDriverWait homePageWait = new WebDriverWait(driver, Duration.ofSeconds(10));
				homePageWait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable((By.xpath("//div[@class='slds-icon-waffle']"))));

				// click the App launcher
				driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();

				WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));

				// Click View All option from the App Launcher expansion
				driver.findElement(By.xpath("//button[text()='View All']")).click();
				
				//wait for app launcher loading
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@type='search' and @class='slds-input']"))));
				
				// Enter the text as Sales in the search and click the Sales Tab
				driver.findElement(By.xpath("//input[@type='search' and @class='slds-input']")).sendKeys("Sales");
				driver.findElement(By.xpath("//p[contains(text(),'Manage your sales')]")).click();
				
				homePageWait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable((By.xpath("//span[text()='More']"))));
				//Click the More Drop down from the Sales page
				driver.findElement(By.xpath("//span[text()='More']")).click();
				
				Thread.sleep(5000);
				//Click the Cases from the More Dropdown
				WebElement casesDropdown = driver.findElement(By.xpath("//span[@class='slds-truncate']/span[text()='Cases']"));
				
				JavascriptExecutor js = (JavascriptExecutor)driver;
				js.executeScript("arguments[0].click();", casesDropdown);
				
				homePageWait.ignoring(NoSuchElementException.class).until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//table[@data-aura-class='uiVirtualDataTable']/tbody/tr[1]/td[7]"))));
				
				//Click the Dropdown from the 1st case
				driver.findElement(By.xpath("//table[@data-aura-class='uiVirtualDataTable']/tbody/tr[1]/td[7]")).click();
				
				
				Thread.sleep(4000);
				//Click the edit option from the dropdown
				WebElement editOptionClick = driver.findElement(By.xpath("//div[@class='forceActionLink' and text()='Edit']"));
	            js.executeScript("arguments[0].click();", editOptionClick); 
	            
	            Thread.sleep(5000);
	            
	          //Change the status from Escalated to Working Status from the Dropdown
	    		driver.findElement(By.xpath("//label[@class='slds-form-element__label']//following-sibling::div")).click();
	    		driver.findElement(By.xpath("//lightning-base-combobox-item[@data-value='Working']")).click();
	
	    		//Change the Priority status to Low using dropdown click
	    		driver.findElement(By.xpath("(//span[text()='Priority']/..)/following-sibling::div")).click();
	    		driver.findElement(By.xpath("//div[@class='select-options']//li[4]")).click();
	    		
	    		//Click the Case Origin
	    		driver.findElement(By.xpath("(//span[text()='Case Origin']/..)/following-sibling::div")).click();
	    		driver.findElement(By.xpath("//a[@title='Phone']")).click();
	    		
	    		//SLA Violation dropdown selection to No
	    		driver.findElement(By.xpath("(//span[text()='SLA Violation']/..)/following-sibling::div")).click();
	    		driver.findElement(By.xpath("//a[@title='No']")).click();
	    		
	    		driver.findElement(By.xpath("//button[@title='Save']")).click();
	    		
	    		WebDriverWait saveCase = new WebDriverWait(driver, Duration.ofSeconds(3));
	    		saveCase.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']"))));
	    		
	    		String caseSuccessMessage = driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText();
	    		
	    		System.out.println(caseSuccessMessage);
	    		
	    		driver.quit();
	    		
	}

}
