package sprint1;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class S06_054_CreateNewCase {

	public static void main(String[] args) throws InterruptedException {

		// Chrome Browser setup using WebDriver Manager
		WebDriverManager.chromedriver().setup();

		// disable the chrome notifications
		ChromeOptions opt = new ChromeOptions();
		opt.addArguments("--disable-notifications");

		// Webdriver Setup by passing chromeoptions
		WebDriver driver = new ChromeDriver(opt);

		// Maximise the Browser Window
		driver.manage().window().maximize();

		// WebDriver wait initialization before the exception
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

		// Get the URL
		driver.get("https://login.salesforce.com/");

		// Enter the Username in the login page
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");

		// Enter the Password in the login page
		driver.findElement(By.id("password")).sendKeys("BootcampSel$123");

		// Click the LogIn button from login page
		driver.findElement(By.id("Login")).click();

		// click the App launcher
		driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[text()='View All']"))));

		// Click View All option from the App Launcher expansion
		driver.findElement(By.xpath("//button[text()='View All']")).click();

		// Enter the text as case in the search and click the result Cases
		driver.findElement(By.xpath("//input[@type='search' and @class='slds-input']")).sendKeys("case");
		driver.findElement(By.xpath("//mark[text()='Case']")).click();

		// Click the New tab from the 2nd top right corner of opened pane
		driver.findElement(By.xpath("//div[text()='New']")).click();

		Thread.sleep(5000);

		// wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//label[@class='slds-form-element__label']//following-sibling::div"))));
        
		//Select the Escalated Status from the Dropdown
		driver.findElement(By.xpath("//label[@class='slds-form-element__label']//following-sibling::div")).click();
		driver.findElement(By.xpath("//lightning-base-combobox-item[@data-value='Escalated']")).click();
		
		//Click and Select the Search Contacts
		driver.findElement(By.xpath("//input[@title='Search Contacts']")).click();
		
		WebDriverWait searchContactDropdownLoad = new WebDriverWait(driver, Duration.ofSeconds(5));
		searchContactDropdownLoad.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@placeholder='Search Contacts...']/following-sibling::div"))));
		
		Actions contactSelection = new Actions(driver);
		contactSelection.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
				
		//Select Email option from the Case Origin Dropdown 
		driver.findElement(By.xpath("//div[@class='test-id__section-content slds-section__content section__content']/div/div[3]/div[2]/div/div/div/div")).click();
		driver.findElement(By.xpath("//div[@role='menu']/ul/li[3]")).click();
		
		//Enter Testing input in the Subject field.
		driver.findElement(By.xpath("(//span[text()='Subject']/parent::label)/following-sibling::input")).sendKeys("Testing");
		
		//Enter Dummy in the Description field.
		driver.findElement(By.xpath("(//span[text()='Description']/parent::label)/following-sibling::textarea")).sendKeys("Dummy");
	
		//Click the Save button.
		driver.findElement(By.xpath("//button[@title='Save']")).click();
		
		WebDriverWait saveCase = new WebDriverWait(driver, Duration.ofSeconds(3));
		saveCase.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']"))));
		
		String caseSuccessMessage = driver.findElement(By.xpath("//span[@data-aura-class='forceActionsText']")).getText();
		
		System.out.println(caseSuccessMessage);
		
		driver.quit();
		
		/*
		 * Thread.sleep(15000);
		 * 
		 * String expectedMessage = driver.findElement(By.
		 * xpath("(//p[text()='Case Number'])[5]/following-sibling::p/slot/lightning-formatted-text"
		 * )).getText();
		 * 
		 * System.out.println(expectedMessage);
		 * 
		 * if(caseSuccessMessage.contains(expectedMessage))
		 * System.out.println("Case Successfully Created"); else
		 * System.out.println("Case not created Successfully");
		 */
		
			
			
	}
	

}
